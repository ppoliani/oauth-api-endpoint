
// region Private Fields

var expressApp,
    passport;

// endregion

// region Inner Methods

var
    /**
     * The actual method that will register the endpoint
     * @private
     */
    _registerEndpoint = function(httpMethod, url, opts, func){
        httpMethod.call(expressApp, url, [
            passport.authenticate('bearer', { session: false }),
            _getScopeGuard(opts.scope),
            _getClaimGuard(opts.claims),
            func
        ]);
    },

    /**
     * Returns a middleware that will check if the scope is permitted
     * @param endpointScope
     * @returns {scopeGuard}
     * @private
     */
    _getScopeGuard = function _getScopeGuard(endpointScope){
        return function scopeGuard(req, res, next){
            var scope = req.authInfo.scope;

            if(scope === endpointScope){
                next();
            } else {
                res.status(401).send('Unauthorized action');
            }
        };
    },

    /**
     * Returns a middleware that will check if the urls claims match those of user
     * @param routeClaims
     * @private
     */
    _getClaimGuard = function _getClaimGuard(routeClaims){
        return function claimGuard(req, res, next){
            var userClaims = req.authInfo.claims;

            for(var routeClaim in routeClaims){
                var routeClaimValue = routeClaims[routeClaim],
                    userCorespondingClaims = userClaims.filter(function(userClaim){ return userClaim.type === routeClaim }),
                    commonClaims = userCorespondingClaims.filter(function(item){ return routeClaimValue.indexOf(item.value) !== -1 });

                if(!commonClaims.length){
                    res.status(401).send('Unauthorized action');
                    return;
                }
            }

            next();
        }
    };

// endregion

var apiEndpoint = function apiEndpoint(_expressApp_, _passport_){
    expressApp = _expressApp_;
    passport = _passport_;

    return {
        get: _registerEndpoint.bind(null, expressApp.get),
        post: _registerEndpoint.bind(null, expressApp.post),
        put: _registerEndpoint.bind(null, expressApp.put),
        patch: _registerEndpoint.bind(null, expressApp.get.patch),
        delete: _registerEndpoint.bind(null, expressApp.delete),
    };
};


module.exports = apiEndpoint;